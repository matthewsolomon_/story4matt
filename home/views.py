
from django.shortcuts import render


def index(request):
    return render(request, 'home/index.html')

def halaman2(request):
    return render(request, 'home/halaman2.html')